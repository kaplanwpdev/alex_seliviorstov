<?php
/*
Template Name: Feed
*/
// Make a page with slug feed results
require_once 'inc/feeds.php';
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
                    <div class="container-fluid">
                        <?php foreach ($feeds as $feed): ?>
                            <div class="col-md-4">        
                            <?php foreach ($feed['list']->get_items(0, 5) as $item): ?>

                                    <div class="item">
                                            <h2 class="title"><a href="<?php echo $item->get_permalink(); ?>"><?php echo $item->get_title(); ?></a></h2>
                                            <?php echo $item->get_description(); ?>
                                            <p><small>Posted on <?php echo $item->get_date('j F Y | g:i a'); ?></small></p>
                                    </div>

                            <?php endforeach; ?>
                            </div>    
                        <?php endforeach; ?>                         
                    </div>               
		</main><!-- .site-main -->
	</div><!-- .content-area -->

<?php get_footer(); ?>
