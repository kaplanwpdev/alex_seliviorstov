<?php
include_once(ABSPATH . WPINC . '/class-simplepie.php');
$feeds = array(
    array(
        'link' => 'http://feeds.bbci.co.uk/news/rss.xml',
        'list' => null        
    ),
    array(
        'link' => 'http://feeds.bbci.co.uk/news/business/rss.xml',
        'list' => null        
    ),
    array(
        'link' => 'http://feeds.bbci.co.uk/news/technology/rss.xml',
        'list' => null        
    )    
);

foreach($feeds as $key => $feed){
    $feeds[$key]['list'] = new SimplePie();
    $feeds[$key]['list']->set_feed_url($feed['link']);
    $feeds[$key]['list']->init();
    $feeds[$key]['list']->handle_content_type();    
}